# AP 2019 RASMUS

***MiniEx03: Infinite Loops***

**URL:** https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx03/Project-miniex03/empty-example/index.html


**What are the time-related syntaxes/functions that you have used in your program? and why you use in this way?**

I have used the translate command instead of the rotate because I was familiar with the translate function and knew how to use the different arguments to make my program. My program captures the essence of wait time being this kind of “shitty” experience that occur when a browser or an application is doing something in the background before starting your intended entertainment in most cases. My throbber uses to image files that is imported into the code, and is display as a “poop” and a “fly”. The fly is circling around the poop and simulating a original throbber in that sense. The concept could be used as a loading screen for a wacky game of some sort or just to prank someone with some fake latency. 


**How is time being constructed in computation (can refer to both reading and your process of coding)?** 

Data packages are traveling through to and from server and sometimes it takes a bit longer to go through the right gates. 
The throbber icon acts as an interface between computational processes and visual communication. When data is being received, loaded or buffering, beyond an acceptable threshold of latency, an animated throbber is displayed on the screen to show the user that something is happening behind the scene. However, the throbber does not have a direct relationship with the data handling and is only a visual indication to show that the screen did not freeze. In my design, I tried to play with the emotional effect of throbber in a funny way. Emotionally, throbber are sometimes annoying or even frustrating in the sense that an unsmooth flow of data make people impatient. In this time, we do not want to wait on a buffer because it is seen as unproductive in that it consumes time.  



**Think about a throbber that you have encounted in digital culture e.g streaming video on YouTube or loading latest feeds on Facebook or waiting a ticket transaction, what do you think a throbber tells us, and/or hides, about?**

How might we think about this remarkable throbber icon differently?
Normally a throbber would indicate that some data is being received and is taking longer than usual, compare to the normal flow of data. When viewing something on a streaming service like YouTube they have started to optimize to make as little wait time as possible then people are watching videos because the run a lot of advertisement usually. They would have less of an audience if people had to wait on first an advertisement and then a throbber to watch a sometimes short video. 

Nowadays a phenomena of “false latency” is becoming more of a reality. In essence, false latency is when someone is holding something back or purposely making latency on the data processing. Sometimes they want to convey a certain image that the content is worth waiting for and therefore making something not instantly load.  However it’s also controversially connect to the fast lanes that a lot is talking about nowadays. Fast lanes is a way to get no latency by paying more money to in your internet subscription, which limits the net neutrality and even the freedom of speech.  
I think that its good with a throbber and a progress bar so we have more different ways to visualize that the user are receiving data. However, I would like to see more design than a spinning circle.


**Screenshot:** 
![screenshot](screenshot.PNG)


