let paperx=210;
let y=110;
let data;
let x=500;
let wood;
let pencil;
let eraser;

function preload(){
  data = loadJSON("voresJSON.json");
  wood=loadImage("assets/woodtable.jpg");
  pencil=loadImage("assets/pencil.png");
  eraser=loadImage("assets/eraser.png");
}

function setup(){
 frameRate(50);
 createCanvas(540,500);
 background(0);
 image(wood,0,37, windowWidth,windowHeight);

}

function draw(){
 rect(200,60, 300,400);
 image(pencil,130,250,400,400);
 image(eraser,95,340,140,140);

 AnAcceptanceSpeechToHonorAGayMan();
 IsItCensorship();
 rect(10,5,518,25);
 RelevantNews();
}

// the "news ticker"
function RelevantNews(){
  push();
  let tale = data.citat;
  text(tale,x,22);
  x=x-1.5;
  pop();
}

function AnAcceptanceSpeechToHonorAGayMan(){
// Headlines
 push();
 textSize(15);
 text('Rami Maleks Acceptance Speech', paperx,90);
 textSize(10);
 text('Oscar for Lead Actor at the 2019 Academy Awards',paperx,105);
 pop()

// Body text
 push();
 textSize(12);
 text('(…) Thank you Queen.Thank you guys for being, for',paperx,y+20);
 text('allowing me to be the tiniest part of your phenomenal',paperx,y+40);
 text('extraordinary legacy. I am forever in your debt. My',paperx,y+60);
 text('crew and my cast I love you. You are my equals,',paperx,y+80);
 text('you are my betters. I could have never been here',paperx,y+100);
 text('without you. I think about, I think about what it would',paperx,y+120);
 text('have been like to tell little bubba Rami that one day',paperx,y+140);
 text('this might happen to him and I think his curly-haired',paperx,y+160);
 text('little mind would be blown. That kid was, he was',paperx,y+180);
 text('struggling with his identity, trying to figure himself out.',paperx,y+200);
 text('And I think to anyone struggling with theirs and trying',paperx,y+220);
 text('to discover their voice: listen, we made a film about',paperx,y+240);
 text('a                       , an immigrant who lived his life just',paperx,y+260);
 text('unapologetically himself. And the fact that I’m',paperx,y+280);
 text('celebrating  him and this story with you tonight is',paperx,y+300);
 text('proof that we’re longing for stories like this (...).',paperx,y+320);
 pop();
}

// the chance in the body text
function IsItCensorship(){

 if (mouseIsPressed) {
      text('special group',paperx+10,370);
    }
 else {
      text('    gay man',paperx+10,370);
    }

  print(mouseIsPressed);
}
