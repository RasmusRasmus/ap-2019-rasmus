# AP 2019 RASMUS

**MiniEx11: ... **


> "The objective in this miniex is to ... " 


**Individual part:**

I choose to revisit my miniex4, which technically for me was the most difficult to produce. This program's process was very linear and relative easy to convert into a flowchart format. The flowchart describes well both the decisions and processes of the program, when running it. 


Here is a hyperlink to the miniex4 folder: https://gitlab.com/RasmusRasmus/ap-2019-rasmus/tree/master/MiniEx04/Project-miniex04



My miniex4 flowchart:  
![screenshot](individualflowchart.png)




**Group part:**

1. Present two different ideas with two different flow charts:

We have chosen two ideas for our forthcoming program for the final project. Our first idea is an E-lit and our second idea is a throbber concerning global warming.

**E-literature:** 
Our first idea is an e-literature concerning gender and sexuality. We have chosen this topic, as we like E-literature in general and like when a program has a political statement. Thus we have chosen to make a new version of a traditional fairytale, where we question the gender of the prince/ princess/ man/ woman, etc. Our idea is to incorporate the code itself into the statement. Down below you can see a simplification of our program in a flowchart: 

![screenshot](groupsflowchart1.png)

**Global Warming:** 
Our second idea concerns global warming. We have chosen to represent global warming by making a throbber circling around an ellipse looking the earth. For each time the throbber reaches the top of the earth, the mainland should become smaller. In the end, all the water/ blue should have overflood the world. Below our throbber and earth, we want to implement an API of Nasa climate change, whereas there should be a live-temperature showing. We have made the simplification of our idea in a flowchart down below:


![screenshot](groupsflowchart2.png)





