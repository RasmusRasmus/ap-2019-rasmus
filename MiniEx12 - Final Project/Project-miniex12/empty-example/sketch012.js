let angle = 0;
let img1;
let img2;
let temperature;
let satellitecartoon;
let count=12;


function preload (){
  img1 = loadImage('space3.png');
  img2 = loadImage('graf.png');
  temperature = loadJSON('temperature.json');
  satellitecartoon = loadAnimation('satellite0.png','satellite11.png');

}

function setup(){
createCanvas(windowWidth, windowHeight);
angleMode(DEGREES);
frameRate(100);
textSize(40);
textAlign(CENTER);
}


function draw(){
image(img1, 0, 0, 1500, 1000);

fill(255)

push();
textSize(20);
text('0',290,895)
text('0',1065,895)
text('1880',320,885)
text('2018',1032,885)
text(temperature.y1880, 320,910)
text(temperature.y2018, 1055,695)
pop();

push();
textSize(35);
text('Global Temperature',690,980);
pop();

push()
scale(0.2)
image(img2,1500,3509)
pop()

Earth();
death();
satellite();
}

function death(){
if (frameCount%720==0) {
  count--;
}
  if (count>0){
   ellipse(665,255, 120, 20);
   ellipse(685,240, 100, 35);
   ellipse(640,247,30,30);
   ellipse(700,247,100,20);
   ellipse(635,270, 20, 80);
}

  if (count>1){
   ellipse(630,252,30,30);
   ellipse(710,230, 100, 30);
   ellipse(640,270, 20, 80);
   ellipse(740,240,40,20);
}

  if (count>2){
    ellipse(620,305, 80, 20);
    ellipse(572,350, 80, 90);
    ellipse(600,370, 100, 80);
    ellipse(665,370, 40, 80);
    ellipse(610,311, 100, 10);
    ellipse(650,340, 30,30);
}

  if (count>3){
   ellipse(600,400, 80, 150);
   ellipse(630,420, 80, 150);
   ellipse(665,400, 50, 150);
   ellipse(780,240,30,10);
}

  if (count>4){
   ellipse(710,582, 100, 70);
   ellipse(870,400, 60, 100);
   ellipse(897,400, 50, 80);
   ellipse(615,500, 20, 70);
}

  if (count>5){
   ellipse(650,550, 50, 70);
   ellipse(632,560, 50, 70);
   ellipse(610,568, 20, 30);
}

  if (count>6){
   ellipse(680,555, 80, 60);
   ellipse(680,575, 110, 70);
   ellipse(660,567, 110, 70);
   ellipse(610,290, 70, 30);
}


  if (count>7){
   ellipse(605,495, 20, 150);
   ellipse(690,350, 60, 60);
   ellipse(690,330, 30, 60);
   ellipse(650,340, 30,30);
}

  if (count>8){
   ellipse(855,410, 30, 80);
   ellipse(897,450, 40, 80);
   ellipse(850,410, 40, 80);
   ellipse(600,500, 20, 70);
   ellipse(870,460, 40, 60);
}

  if (count>9){
   ellipse(900,480, 20, 30);
   ellipse(865,325, 20, 40);
   ellipse(874,315, 40, 30);
   ellipse(650,275, 30, 30);
   ellipse(620,520, 30, 30);
}

  if (count>10){
   ellipse(898,485, 20, 30);
   ellipse(902,420, 40, 110);
   ellipse(570,450, 20, 100);
   ellipse(590,490, 40, 50);
   ellipse(650,520, 50, 10);
}

  if (count>11){
   ellipse(700,580, 110, 70);
   ellipse(620,555, 60, 60);
   ellipse(700,340, 80, 50);
   ellipse(800,260,15,10);
   ellipse(720,255, 100, 20);
}
 }

function Earth(){
//The Earth
noStroke();
fill(0,0,255); //the colour of the globe
circle(720,415,202);
//the "mainland"
fill('hsb(160, 100%, 20%)');//the colour of "mainland" on the globe
}

function satellite(){
translate(720,415);
rotate(angle);
fill(255);
rectMode(CENTER);
scale(0.045);
animation(satellitecartoon,4500,4500);
angle = angle + 0.5
}
