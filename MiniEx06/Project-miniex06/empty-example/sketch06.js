let bombfall1;
let bombfall2;
let bombfall3;

let img;
let img1;
let img2;

function preload () {
  img=loadImage('assets/kurv.png');
  img1=loadImage('assets/windowshill.jpg');
  img2=loadImage('assets/bomb.png');
}

function setup() {
  createCanvas(1200, 620);
  bombfall1 = new enemy();
  bombfall2 = new enemy();
  bombfall3 = new enemy();

}

function draw() {
  image (img1,0,0);
  basket();
  bombfall1.move();
  bombfall1.show();
  bombfall2.move();
  bombfall2.show();
  bombfall3.move();
  bombfall3.show();


  function basket(){
    imageMode(CENTER);
    img.resize(100, 100);
    image (img, mouseX , 500 );
    noCursor();
   }

}

class enemy{
  constructor(x,y,speed) {
    this.x = random(width);
    this.y = 0;
    this.speed = random(1, 6);
  }

  move() {
    this.y +=  this.speed;;
  }

  show() {
    image (img2, this.x , this.y);
    img2.resize(50, 50);
  }
}
