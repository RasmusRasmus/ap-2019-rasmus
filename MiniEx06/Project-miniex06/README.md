# AP 2019 RASMUS

**MiniEx06: Games with objects**

URL: https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx06/Project-miniex06/empty-example/index.html

> "The objective of this assignment were to implement a class-based object oriented sketch via abstracting and designing objects' properties and behaviors and to reflect upon object orientation in the domain of digital culture.  

In this MiniEx i have tried to make my very own game, with some simple aspects. 

**• Describe how does your game/game object work?**
My program is a third version of a prototype to a game involving catching bombs before they hit the ground. This one is a more simplified version of my original tries and my original intensions, however, after many attempts i settled with this one, and scraped the others, because i couldn't get the code to work and getting all of the components to interact with each other on the others. This means the first and the second try had more elements including in the code, like bomb falling repeatedly and collision with the basket so they disappear. I tried to simulate that more bombs would spawn with just copying some functions, so that at the start of the program there is 3 bombs spawning. There isn't any interactivity because of time limitations. 

Mainly i used most of the time trying to figure out how object orientated programming worked. So my coding this time isn't that useful for a peer feedback session, because it was more of a learning process for me personally, which unfortunately wasn't available to be shown in the final program. I might end up returning to finish the game in the future because of perfectionism. 

**• Describe how you program the objects and their related attributes and methods in your game**
My initial approach were to make different "baskets" or objects to catch the falling bombs. However, it made more sense to use the bombs as an objects with different attributes like spawning positions at the top on the x axis and the downward speed they are falling with on the y axis. In this way I can manipulate the gameplay and make configure the right difficulty for the player. As this game is only a prototype, there could been have added at lot of extra elements like score, stages or obstacles to enhance the player experience. 

**• Based on Shiftman's videos, Fuller and Goffey's text and in-class lecture, what are the characteristics of object-oriented programming?**
Compared to the previous way i personally, and maybe others in the class, have structured our code is something called procedural programming, which in its essence is where you write function for everything and something needs to copy and paste for of the same code later in the code. This way of programming isn't very practical when ones coding skills or capabilities evolve and therefore the complexity of one's code increases, because the functions might end up impact each other. Object oriented programming tries to fix this issue by grouping related functions and associating data into a unit, usually called an object. This procedure is a way to organize one's code, not just for humans but also when the computer are reading the code. Furthermore it's possible to conceal more intricate and confusing attributes to an objects behavior, because you can just call the object when you need it, instead of declaring and using a objects attributes every time. So one characteristics is that one encapsulates a objects attributes to a single class that is easily called elsewhere in the code. Additionally, when you are making an object in a code is essentially to 

> "capturing and focusing on the important details, while leaving out those that are less important" (Lee 2013, p. 18).  

Avoiding unnecessary elements we reduce the impact of change in the code and ensures less unwanted cross interaction in the code. It can also be seen as a blueprint or template about capability of what the specific objects can do. 


**• Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?** What are the implications of using object oriented programming?
When you are trying to describe an object or item, you are doing abstractions in object oriented programming that can hide irrelevant details of that object, so your object only contains the necessary elements. However, i don't really understand this question though :P


**Picture 1:**

![screenshot](program.PNG)



**Picture 2:**

![screenshot](program2.PNG)

