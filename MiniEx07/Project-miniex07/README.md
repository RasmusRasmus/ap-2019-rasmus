# AP 2019 RASMUS

**MiniEx07: A generative program**

URL: https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx07/Project-miniex07/empty-example/index.html


> "The objective of this assignment were to implement a rule-based generative program from scratch, to strengthen the computational use of loops and conditional statements in a program and to reflect upon the concept of generativity conceptually and practically, such as systems, rules, temporality, autonomy/authorship, liveness, emergence and processes." 

In this Miniex i worked together with Freya Wattez (@Freyaw) and we took inspiration in Winnie Soon's version of the Langton's ant. We used a group members face instead of pixels moving in a grid. Link to Winnie's: https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/class07/sketch07/sketch07.js

***• What are the rules in your generative program and describe how your program performs over time. What have been generated beyond just the end product?***

We considered many different options of rules we could implement in our program. However the rules we ended up with was first and foremost when the grid position is equal to 0, the "ant" or the face in this case needed to turn right 90 degrees. The second rule to our program is if the ant goes left go one up, or else turn 90 degrees to the left. The last rules is when you press the mouse you can make the ant go its starting position, which essentially is a restart of the whole program. 
The program only places the face when it has visited the green spot, so when it goes on the spots again the face is already on the location and the ant disappears for a little while. Even though it looks like it disappears for a while it's still moving, just behind the scenes. We have tried to work with Winnie's program to try to incorporate our own spin and our own ideas to give it a funny twist. 

***• What's the role of rules and processes in your work?***

There are no random values to our rules, so the program will essentially generate the same outcome every time you run it. We implemented the reset button to let the viewer easily restart the program, when analyzing. The rules makes the ant travel around in a grid and as show on the pictures it starts out as small centered smudge and turns out as a cross later in the programs process. The ant travels slowly compared to Winnie's program, but that was intentionally because we wanted to show its progression for the viewer.

***• What's generativity and automatism? How does this mini-exericse help you to understand what might be generativity and automatism? (see the above - objective 3 and the assigned readings)***

You give the program some conditions that it needs to follow and the generative or automative aspect is that the program generates the outcome based on the conditions given. If we have entered random values to the program, the computer would make different patterns every time, which us human can't predict. This program could be called pseudorandom, because at first glans it looks like a random process, however the ant is only following simple rules that predictable.

![screenshot](screenshot2.PNG)


![screenshot](screenshot1.PNG)

