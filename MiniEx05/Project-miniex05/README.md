# AP 2019 RASMUS

**MiniEx05: Return**

URL: https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx05/Project-miniex05/empty-example/index.html


> "The objective of this assignment were to catch up and explore further with the previous learnt functions and syntaxes, to revisit the past and design iteratively based on the feedback and reflection on the subject matter and to reflect upon the methodological approach of "Aesthetic Programming" 

In this miniex5 we should visit the past syntaxes and references we have learn and try to either to construct something new out of already written code or to write a whole new program on the basic on our current knowledge repertoire. 
Because i didn't have that much time this week, i decided to play around with one of the exampels in the p5.js library. I have taken inspiration from the "snowflakes": https://p5js.org/examples/simulate-snowflakes.html example and tinkered with the code to alter it into a new thing. 

**Based on the readings that you have done before (see below), What does it mean by programming as a practice?:**
The essence of programming as a practice has many different branches of usages and it depends on the way you think about it. If you interpretate it in an educational context the meaning is to educate the general public and mostly children to learn the ways of programming. However mostly its used in a context of exploring new ideas with programming as a develop- and brainstorming tool. Programming as a practice also involve things like individual empowerment to the extent that you have the confidence to set goals and complete these goal, by tinkering or experimenting in different contexts. 

**To what extend do you agree the concepts that have been illustrate in below readings? (Can you draw and link some of the concepts in the text and expand with your critical take?)** The readings touches upon a general agreement on some of the elements in programming. Especially how practices of programming apply and how to argue about why one should be able to code.

**What is the relation between programming and digital culture?:**

Programming is the foundation of digital culture and the newly internet of things. Without programming and coding it would have never been made and in the future its becoming more and more important. More and more daily things are becoming technologized and getting connected to the internet, which have programming following with. 
Even though programming is becoming more and more fundamental embedded into our society with modern technology becoming more available. I think that the future ability of coding and programming will become a literacy just like reading and writing is today, it's something that is good to know how to read the code for different purposes and reading the different syntaxes are becoming more essential. However I don't think that everyone need to know how to code long programs, because we still need people with different professions to have a running society. Robots and algorithm might overtake a lot of assignments but i don't think that human labour, with its critical thinking and common sense, can be replaced by a whole technological world.



Picture 1: ![screenshot](KODE.PNG)


Picture 2: ![screenshot](PROGRAM.PNG)

