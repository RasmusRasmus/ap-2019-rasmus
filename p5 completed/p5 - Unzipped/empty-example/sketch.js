function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  background('#4d94ff');
  frameRate(40);

}
  function draw() {
  background('#4d94ff');
  rotateX(frameCount * 0.03);
  rotateY(frameCount * 0.03);

  fill('#ffff99')
  translate(100, 0, 50);
  box(5,30, 5);

 fill('#ff4da6');
 translate(-100, 0, -50)
 torus(100,50,30,30);
}
