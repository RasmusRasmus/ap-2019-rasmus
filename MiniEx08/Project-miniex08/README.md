# AP 2019 RASMUS

**MiniEx08: Generate an electronic literature**

URL: https://glcdn.githack.com/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx08/Project-miniex08/empty-example/index.html


> The objective in this miniex is to design and implement an electronic literature that utilizes text as the main medium (but text can be also manifested in various forms), To learn to code and conceptualize a program collaboratively and to reflect upon the aesthetics of code and language. 


In this miniex i collaborated with @amalienyegaard on a program that would visualize our reflections and thoughts around electronic literature. The start of our process started with different ideas both technically and conceptually and our first concept we tried to construct didn't work technically as intended. So instead of submitting an incomplete and nonfunctioning program, we choose another route. Eventually we decided to go with an idea that were more simple but visualized our conceptual thoughts to an almost equal extent, which included more syntaxes we were familiar with and knew exactly how to use. Therefore, we didn't explore and experiment with new syntaxes and computational structures  in this miniex, because we didn't want to lose conceptual value. 


**What's the program about and how's the collaborative process?**

Our program has a paper with some text on it laying on a wooden table with a pencil and eraser next to it. On the paper is section of Rami Maleks acceptance speech at the Oscars 2019 for his win in the category "Lead actor", in the movie "Bohemian Rhapsody", the story of the legendary rock band Queen and their lead singer Freddie Mercury. The idea is to show light on some recently controversy of the Chinese media organizations (specifically Mango TV) in relation to the LGBT community. In Mango TV's, an chinese internet enterprise, webcast of the Oscars 2019, they reportedly replaced “gay man” with the phrase “special group.” (Link to an supporting article: http://time.com/5539590/china-censor-rami-malek-oscar-speech/). Our programs concept is that the viewer are assigned the task to chance the phrases.

Additionally we wanted to make a "news ticker" run in the top of the program, to have focus on the relevancy element of this program, running with news from an relevant article that explains the story but translated into Chinese. (link to the article: https://www.reuters.com/article/us-china-censorship-bohemianrhapsody-idUSKCN1R80GZ)

The only functionality of our program, is the ability to click and hold the mouse to chance the words. We decided to use the mousePressed syntax because we wanted to emphasize on the significance difference the two phrases had on the text and we wanted the viewer to easily see the two texts.

**Analyze your own e-lit work by using the text 'Vocable Code', and you can also choose to incorporate any text with the theme of 'language' in software studies class?**

As our program didn't contain any elements from Winnies example 'Vocable Code', like using human voices to elaborate your ideas, we have more of semiotic elements. A computer is a machine that has a specific way of reading command, and if not done properly will give an error and the code will not be executed. Contrary with humans, we can see through small mistakes in the structural semiotic composition of a language without losing the understanding and readability. However, the binary computer is thus better for larger and more complex constructions of data or information, because it can have a more "brain" power than the human. 


**Try to approach this question: what is the aesthetic aspect of your program in particular to the relationship between code and language?**

Language is a way of expressing a message, whether it is artificial language or natural language. In the artificial language here may be some underlying meanings that support the conceptual significance of the program. In our "generic" code we have tried exactly that, by naming our function something that hopefully gets the viewer to generate reflections beyond the code in connection to our discussed theme. Our goal is to experiment with coding, which is a medium to help visualize your thoughts, by adding more value to the code itself rather than just some syntaxes that produce an outcome. The outcome shouldn't always be the most value or desired part of a program with the most meaning. 




![screenshot](screenshot1.PNG)
![screenshot](screenshot2.PNG)


