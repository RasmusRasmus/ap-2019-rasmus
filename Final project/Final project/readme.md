Based on my mini_ex6: https://gitlab.com/Annebeltz/ap2019/tree/master/Mini_ex6

![Screenshot](Skærmbillede_mini_ex10.png)

I chose to visualize my mini_ex6 which is a game. You play the game by pressing the mouse button. In order to avoid the spider webs, you sometimes have to hold the mouse down. My goal was to make some kind of game over when you either touched the spider webs or flew over or under the canvas. My coding skills did not reach this goal, unfortunately. Because of this missing feature, you can play the game into infinity. That was not my intention. In the flowchart, I pretended that my program was able to play "game over" when some of the above scenarios happened. 

I made a flowchart with the intention of making it more simple and easy for a "non-coder" to understand. In terms of this, I only placed the visuals and interactions in the flowchart, just like a manager or designer would have done before giving it to the programmer for the programmer to code it. In drawing a flowchart a lot can go wrong. Even though it is a way of communicating and planning as well as organizing the different groups that have to cooperate can misunderstand each other and thereby the flowchart too. This is an aspect that You need to be aware of when making a flowchart - it has to be understandable and not too complex, but still complex enough to visualize the program or ideas. 
