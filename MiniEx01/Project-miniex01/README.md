# AP 2019 RASMUS

**Describe your first independent coding process (in relation to thinking, reading, copying, modifying, writing, uploading, sharing, commenting code)?**
My first independent coding process was at the start very confusing for me and I tried different ideas, however they didn't work as I initially intended. So i went with my Simpsons doughnut idea :doughnut:, which features a spinning torus in the colorways of the show with one single sprinkle. With time limitations i only went with one sprinkle so it at least the idea would be visualized. 


**How your coding process is differ or similar to reading and writing text? (You may also reflect upon Annette Vee's text on coding literacy)?**
Trying to identify the underlining structure of a "sentence" or a code line, is much like the same as trying to do grammar like spelling and punctuation. Both have fundamental rules which apply to make coherence and intelligible for both humans and computers. You need a high taxonomy level to code, becuase you need to know how to read it, analyse its component and use it in new ways which according to fx. Bloom’s Taxonomy. 


**What is code and coding/programming practice means to you?**
I haven't tried to code anything before this moment in time, despite my interest in computers. However, i find it very interesting as a way to visualize ideas and play with designing aspects that is not possible to make with a pen and paper, because of the high abstraction level that a computer can produce. 


**URL:** https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx-1/Projektet_miniex-1/empty-example/index.html




**Billeder:**

![screenshot](miniex-1-screenshot-programmet.PNG)
![screenshot](miniex-1-screenshot-koden.PNG)