let input, button, greeting, you;
let knap;

function setup() {
//create canvas
  createCanvas(windowWidth, windowHeight);

//Load input
  input = createInput();
  input.position(windowWidth/2, windowHeight/2);

// create the submit button
  button = createButton('submit');
  button.position(input.x + input.width, windowHeight/2);
  button.mousePressed(greet);

// Create the name element
  greeting = createElement('h2', 'Enter your name');
  greeting.position(windowWidth/2, windowHeight/2.4);

//text modifier
  textAlign(CENTER);
  textSize(20);
}


//the text function
function greet() {
  const name = input.value();
  greeting.html('Hello ' + name + '!' + ' Anyone else?');
  input.value('');

// number of name repeats printed
  for (let i = 0; i < 150; i++) {
push();
    fill(random(255), random(255), random(255), random(255));
    translate(random(width), random(height));
    rotate(random(2 * PI));
    text(name, 0, 0);
pop();
}

// number of webcams loading
for (let webcam = 0; webcam < 1; webcam++) {
capture=createCapture(VIDEO);
capture.size(100,100)
capture.position(random(windowWidth),random(windowHeight));
}


}
