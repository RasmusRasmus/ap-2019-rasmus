# AP 2019 RASMUS

**MiniEx04: Data capture**

The goal was to experiment with various data capturing inputs including audio, mouse, keyboard, web camera and beyond and to critically reflect upon the activity of data capture in digital culture (beyond buttons).

**URL:** https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx04/Project-miniex04/empty-example/index.html


**Describe about your sketch and explains what have been captured both conceptually and technically. How's the process of capturing?:**

I have tried to experiment with buttons and webcam capture. I took inspiration to the p5.js DOM library with this piece of code. I use some element of the code but changed the values and added additionally elements. The link to the code i experimented with: https://p5js.org/examples/dom-input-and-button.html

My code tried to visualize and be a metaphor for the digital footprint everyone leaves on the internet. When writing your name into the simple box and clicking the submit button, it instantly changes to your name over the whole screen to symbolize that your name behind the scenes end up in different places and places that you didn't expect it to end up. Furthermore, the visualization symbolizes with the amount of names simulated after submitting that different companies and individuals have the opportunity to take some of you "data", in this case your name. I also used the "createCapture(VIDEO);" syntax to place a picture of yourself then you have entered your name in the box, a random place on the screen to symbolize that the world is watching you. 


**Together with the assigned reading and coding process, how might this ex helps you to think about or understand the data capturing process in digital culture?:**

When everyday people often called "users" browse the internet and they leave a digital footprint for the site owner and third party applications to collect for selling or trading. This means that the digital footprint of a user gets an assigned value in real life money and usually they use the data to focus their resources on optimizing their advertisements. Therefore data capturing is an inevitable part (to some extent) of the digital culture or even the developed human society in its basic essence. I wanted to visualize how then you use a program, application or social media and you use your real name, they get branched out to a lot of different places, which allows people to easily collect the data and use it. Digital footprint in this code is an active action for the viewer, however, many times people leave passive footprints, which they have control over. It's a very controversial effect, when you are browsing the internet you need to actively disallow third party tracking with browser add-ons like for example Ghostery.  




**Screenshots:**

![screenshot](screenshot.PNG)
![screenshot](kode.PNG)






