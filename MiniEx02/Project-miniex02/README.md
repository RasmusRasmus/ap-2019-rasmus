# AP 2019 RASMUS
*MiniEx 2: Geometric emoji*

I have made two emojis in two different programs. 


**Describe your program and what you have used and learnt:**
Both of my programs are in their bacis essence based on the idea that emojis should focus on the emotionel value that they originally were invented with. Both of the programs have a yellow-emoji ellipse form that is the basis for many emojis hence why i choice that. On the first emoji (link 1) you can draw your current emotionel state and send it to the people you are communicating with. On the second emoji (link 2) you can tap the emoji and random symbols will show many different forms of emotions. 
I learned how to use new preferences from the p5.js library and i learned some new coding methods for visualizing the same idea.  


**What have you learnt from reading the assigned reading?:** 
After i have read the text i have gotten a better understanding of what ASCII and Unicode means, both in how they are different and how much they each can contain of symbols and characters. I especially found the it interesting that Unicode can be up to 32 bits, which can hold up to 2.147.483.647 possible characters. That is insane. Also i found out that the Fitzpatrick scale, is a numerical classification schema for human skin color and the the Fitzpatrick scale is used for the basis of skin color in emoji, with 5 modifiers. In my programe i only used the yellow emoji type, which have been the standard skin tone color for emojis. Lastly it was eye opening how the cultural/social pressure for more difference in emoji ethnicity made the big companies like apple and google use resources on rethinking how emojis represent the human kind and how emotions werent the only thing counting the emojis any more.


**How would you put your emoji into a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond?:**

The inpiration for my emojis is grounded in the cultural pressure to make emojis more than a visualizer of the emotions a person want to send and in the way many of the big companies have added the different skin colors to the emojis. My emoji designs take the standared yellow skin color and makes it the only type of skin color available to use. However, the freedom to draw your own customized emojis in the moment to show your current emotions, opens new possibilities in the texting universe. 
Also my second emoji is more like gifs, were you look a random emotions until you find the perfect one that best shows your emotions. Its not guaranteed that you get some good facial expressions in the first try, so you are forced to look at different combinations before you find the right one, which makes you think more about what you send. So its not your first instinct that you send, which sometimes is best for both the sender and receiver if the sender sends something bad. 

***Emoji 1:*** https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx02/Project-miniex02/empty-example/index02_Emoji1.html
![screenshot](Emoji1-Screenshot.PNG)

***Emoji 2:*** https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx02/Project-miniex02/empty-example/index02_Emoji2.html
![screenshot](Emoji2-Screenshot.PNG)



