//facial expressions
var mouth = ["0","?","o","Q","/","£","#","ʎ","𐅀","Ͼ","ೞ","၇","ዖ","ዕ","ゝ","ん","∩","〆","$","¤","^","´","+","ܤ","ౠ","෴","ᇹ","᎑","ᗐ"];
var lefteye = ["0","?","o","Q","/","£","#","ʎ","𐅀","Ͼ","ೞ","၇","ዖ","ዕ","ゝ","ん","∩","〆","$","¤","^","´","+","ܤ","ౠ","෴","ᇹ","᎑","ᗐ"];
var eyeright = ["0","?","o","Q","/","£","#","ʎ","𐅀","Ͼ","ೞ","၇","ዖ","ዕ","ゝ","ん","∩","〆","$","¤","^","´","+","ܤ","ౠ","෴","ᇹ","᎑","ᗐ"];

function setup() {
createCanvas(500,500)
background(240,30,120);
fill('#ffff0a');
ellipse(250,270,350,350);
fill(0);
textSize(23);
text('VISUALIZE YOUR EMOTIONS RANDOMLY', 20,50);
fill(25,54,255)
rect(100,450,310,30)
fill(0)
textSize(23);
text('Share it with the world', 137,475);

}

// random facial expressions used on the emoji
function mousePressed() {
	clear()
	setup()
fill(0)
	textSize(45)
var randomword = random(eyeright);
text(randomword, 320,200);

randomword = random(lefteye);
text(randomword, 170, 200);

randomword = random(mouth);
text(randomword, 240, 350);
}
