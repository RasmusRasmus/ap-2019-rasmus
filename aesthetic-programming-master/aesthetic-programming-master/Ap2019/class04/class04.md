#### Class 04 | Week 9 | 26 Feb 2019: Data capture
##### With Wed tutorial session and Fri shutup and code session.

### Messy notes:

#### Agenda:
1. Contextualizing the theme with the project showcase: How We Act Together by Lauren McCarthy and Kyle McDonald
2. Peer-tutoring: p5.dom libary
3. Data capture with Mouse and Keyboard events
4. Data capture with Web camera inputs: with clmtrackr.js library
5. Data capture with audio inputs: with p5.sound library
6. Walkthrough next mini-ex4: Capture All
---
### 1. Contextualizing the theme with the project showcase: How We Act Together by Lauren McCarthy and Kyle McDonald
[background video](https://loopvideos.com/SPjtkUs1rvA?from=326&to=454)

![img](http://payload496.cargocollective.com/1/19/625408/12229985/scream-comp.gif)

*How We Act Together* focuses a lens on the small gestures of social interaction, asking participants to repeat them until exhausted, to a point where the gesture no longer feels natural and its meaning begins to shift. Interrogating a gesture through repetition evokes the discomfort we sometimes encounter in social interactions in the wild. We are becoming increasingly familiar with conversations characterized by divided attention and a loss of connection.

But the gesture is not completely empty, as the participant’s performance triggers a response. For as long as the participant acts, they are able to view a stream of all the previous participants performing the same action back at them. Distributed across a crowd, spread over space and time, an asynchronous interaction takes place that is both awkward and intimate.

The entire interaction is choreographed by the software. Participants act on command from the screen, and their performance does not begin until they conform to the metrics of computer vision algorithms that recognize their gesture. The software validates their human expressions and movements, allowing them access to the crowd of people interacting together. - Lauren and Kyle
- [Project site](http://lauren-mccarthy.com/How-We-Act-Together)
- [RUNME](https://hwat.schirn.de/) (work on Google Chrome only)
- source code here: https://github.com/kylemcdonald/HowWeActTogether-Tracking
- [30 mins Video](https://www.youtube.com/watch?v=SPjtkUs1rvA)

"In a society ruled by algorithms, data is always at play. When quantification becomes part of our daily routines, we more or less consciously contribute to a permanent capture of life into data. Value can now potentially be extracted from everything, and productivity can be measured in all fields of life. This particularly redefines the relation between work and play as ludic and participatory structures are purposefully used to render personal information traceable, social relationships and behavioural patterns recognisable. Life is increasingly governed by a logic of CAPTURE ALL as a never-ending enterprise of predictive control."  - [Transmediale, 2015](https://transmediale.de/content/capture-all)

"We produce, share, collect, archive, use and misuse, knowingly or not, massive amounts of data, but what does its “capture” do to us? What are the inter-subjective relations between data-commodity and human subjects?"  - [Andersen & Cox, 2015](http://www.aprja.net/datafied-research-introduction/)

#### The concept of capture

#### heat map
![facebook1](https://c.slashgear.com/wp-content/uploads/2013/10/facebook_heat_map.jpg)

[src](https://www.slashgear.com/facebook-investigates-tracking-users-cursors-and-screen-behavior-30303663/)

- [heat map](https://heatmap.me/)
- also links to the field of data visualization and marketing

#### typing
Facebook has the ability to track unposted status updates (what you are typing as you're typing, regardless it is a published/public post or not)
- more: [Self censorship on Facebook by Sauvik Das and Adam Kramer](https://www.aaai.org/ocs/index.php/ICWSM/ICWSM13/paper/viewFile/6093/6350). See methodology: "We collected self-censorship data from a random sample  of  approximately  5  million  English-speaking  Face- book users who lived in the U.S.
or U.K. over the course of 17 days (July 6-22, 2012)."

#### buttons
Facebook buttons:

![facebook2](http://cdn1.sbnation.com/assets/3523775/facebook_like_designs.png)

<img src="https://bloximages.chicago2.vip.townnews.com/dailyitem.com/content/tncms/assets/v3/editorial/3/01/30160806-db02-11e5-af04-233c431ac726/56cdbcf9b4d60.image.jpg" width="450">

#### options
![fb_gender11](fb_2014_1.jpg)

ref: https://www.telegraph.co.uk/technology/facebook/10930654/Facebooks-71-gender-options-come-to-UK-users.html

![fb_gender1](fb_2014.png)

**drop-down menu - The interface of Facebook in 2014**
Ref: https://slate.com/technology/2014/02/facebook-custom-gender-options-here-are-all-56-custom-options.html

![fb_gender0](Picher.png)

A hack by Facebook user Rae Picher

![fb_gender00](article.png)

Ref: Bivens, R. (2017). The gender binary will not be deprogrammed: Ten years of coding gender on Facebook. New Media & Society, 19(6), 880–898. https://doi.org/10.1177/1461444815621527

![fb_gender2](gender_options.png)

Ref: Bivens, R. (2017). The gender binary will not be deprogrammed: Ten years of coding gender on Facebook. New Media & Society, 19(6), 880–898. https://doi.org/10.1177/1461444815621527

interesting stuff here by Sabrina Fonseca: https://uxdesign.cc/designing-forms-for-gender-diversity-and-inclusion-d8194cf1f51

#### analytics
Google analytics:

![google](https://cdn.adventuretravelnews.com/wp-content/uploads/2012/01/google-analytics-services.jpg)

https://www.youtube.com/watch?v=mPgAug15bcQ

#### Voice
![google](http://i.dailymail.co.uk/i/pix/2017/11/14/12/4655673600000578-5080971-image-a-7_1510661012565.jpg)

#### tracker
<img src="sleepTracker.png" width="300">

Fitness and health tracker, sleep tracker,etc

#### Facial/object recognition
- Tagging Photos
- [Next-level Surveillance: China Embraces Facial Recognition](https://www.youtube.com/watch?v=Fq1SEqNT-7c)
- Company: Spyhuman: https://spyhuman.com/pages/wechat-tracker.html

#### Recommendation engines:

<img src="amazon.png" width="550">

---
3-5. Data capture with sample code

![sketch04](sketch04.gif)

[Runme](https://glcdn.githack.com/siusoon/aesthetic-programming/raw/master/Ap2019/class04/sketch04/index.html)    
[Source Code](https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/class04/sketch04/sketch04.js)

- **Capturing mouse events**: p5.dom: css styling (need the p5.dom library)

```javascript
var button;
button = createButton('like');
button.style("display","inline-block");
button.style("color","#fff");
button.style("padding","5px 8px");
button.style("text-decoration","none");
button.style("font-size","0.9em");
button.style("font-weight","normal");
button.style("border-radius","3px");
button.style("border","none");
button.style("text-shadow","0 -1px 0 rgba(0,0,0,.2)");
button.style("background","#4c69ba");
button.style("background","-moz-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
button.style("background","-webkit-gradient(linear, left top, left bottom, color-stop(0%, #3b55a0))");
button.style("background","-webkit-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
button.style("background","-o-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
button.style("background","-ms-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
button.style("background","linear-gradient(to bottom, #4c69ba 0%, #3b55a0 100%)");
button.style("filter","progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c69ba', endColorstr='#3b55a0', GradientType=0 )");
```

```javascript
button.mousePressed(clearence);  
//click the button to clear the screen

function clearence() {
  clear();
}
```
*Pls explore other [mouseEvents](https://p5js.org/reference/) e.g mouseIsPressed, mouseMoved(), mouseDragged(), mouseReleased(), mouseClicked(). doubleClicked(), mouseWheel()*

- **Capturing keyboard events**:

```javascript
function keyPressed() {
  if (keyCode === 32) { //spacebar - check here: http://keycode.info/
    button.style('rotate', 180);
  } else {   //for other keycode
    button.style('rotate', 0);
  }
}
```
*Pls explore other [keyboardEvents](https://p5js.org/reference/) e.g keyIsPressed, key, keyCode, keyPressed(), keyReleased(), keyTyped(), keyIsDown()*

- **Capturing audio events**:(need the p5.sound library)

```javascript
var mic;

function setup() {
// Audio capture
mic = new p5.AudioIn();
mic.start();
}

function draw() {
//sound capture
let vol = mic.getLevel(); // Get the overall volume (between 0 and 1.0)
button.size(floor(map(vol, 0, 1, 40, 500)));  //map the mic vol to the size of button: check map function: https://p5js.org/reference/#/p5/map
}
```
- **Capturing Web Camera events**: (need the clmtrackr.js and model lib)

    - see [clmtrackr](https://github.com/auduno/clmtrackr)

    ![positions](https://www.auduno.com/clmtrackr/examples/media/facemodel_numbering_new.png)

```javascript
var ctracker;

function setup() {
//web cam capture
var capture = createCapture();
capture.size(640,480);
capture.position(0,0);

//setup tracker
ctracker = new clm.tracker();
ctracker.init(pModel);
ctracker.start(capture.elt);
}

function draw() {

var positions = ctracker.getCurrentPosition();
if (positions.length) { //check the availability of web cam tracking
    button.position(positions[60][0]-20, positions[60][1]);  //as the button is too long, i wanna put it in the middle of my mouth, and 60 is the mouth area (check lib spec)
    for (let i=0; i<positions.length; i++) {  //loop through all major face track points
       noStroke();
       fill(map(positions[i][0], 0, width, 100, 255), 0,0,10);  //color with alpha value
        ellipse(positions[i][0], positions[i][1], 5, 5);
    }
}
}
```

##### Discussion:
1. Explore the sample code and to understand what it does
2. What is "like economy" (Gerlitz and Helmond 2013)? What's the affordances of a "button" (Pold 2008)? How do these linkages of a button with metrics, capture and economy could be expanded beyond mere Facebook? What are other forms of capture that you might be aware of?

---
#### 6. Walkthrough next mini-ex4 and next week
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex4
- Peer-tutoring: Group 3 / Respondents: Group 4, Topic: Image+Video/Audio in p5.js
